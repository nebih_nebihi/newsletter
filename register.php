<!doctype html>
<html lang="de-DE">
<head>
    <meta charset="UTF-8">
    <title>Newsletter App</title>
    <link rel="stylesheet" href="assets/vendor/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

<section id="page" class="container">


    <div class="content_register">
        <form class="register" action="danke.php" method="post">
            <fieldset>
                <input type="radio"  class="styled1" name="gender" value="male" alt="gender" required>Herr
                <img id="radio1" src="assets/images/checkbox.jpg" alt=""/>
                <img id="radio1_checked" src="assets/images/gecheckt.jpg" style="display: none;" alt=""/>
                <input type="radio"  class="styled2" name="gender" value="woman" required>Frau
                <img id="radio2" src="assets/images/checkbox.jpg" alt=""/>
                <img id="radio2_checked" src="assets/images/gecheckt.jpg" style="display:none;" alt=""/>
                <br/><br/>
                <input type="text"  name="name" alt="name" placeholder="Vorname" data-parsley-validate required><br/><br/>
                <input type="text"  name="lastname" alt="lastname" placeholder="Nachname" data-parsley-validate required><br/><br/>
                <input type="email" name="email" alt="email" placeholder="E-Mail" data-parsley-validate required><br/><br/>
                <input id="agbs" type="checkbox" name="agbs" style="margin-right: 5px;" required/><a href="http://www.schaebens.de/de/unternehmen/agbs" target="_blank">AGBs</a> gelesen und bestätigt.
            </fieldset>
        <button type="submit">Jetzt bestellen<p class="arrow"></p></button>


        </form>



    </div>



</section>





<script src="assets/vendor/js/jquery.js"></script>
<script src="assets/vendor/js/bootstrap.js"></script>
<script src="assets/vendor/js/parsley.js"></script>
<script src="assets/vendor/js/parsley.remote.js"></script>
<script src="assets/js/myscript.js"></script>

<div id="fb-root"></div>
<script type="text/javascript">
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '610544555699642', // App ID
      channelUrl : '//www.mywebsite.com/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    FB.Canvas.setAutoGrow();
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/de_DE/all.js";
     d.getElementsByTagName('head')[0].appendChild(js);
   }(document));
</script>
</body>
</html>

