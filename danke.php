﻿<?php
header('Content-type: text/html; charset=utf-8');  

include 'system/db.php';

$encoded_params = "";
// Get contact data
if ( isset($_POST['gender']) )
	$gender = $_POST['gender'];
else $gender = "male";
$encoded_params = "gender=".$gender;

if ($gender == "male")
	$salutation = "Lieber";
else $salutation = "Liebe";

if ( isset($_POST['name']) )
	$first_name = $_POST['name'];
else $first_name = "Abonnent";
$encoded_params .= ";first_name=".$first_name;

if ( isset($_POST['lastname']) )
	$last_name = $_POST['lastname'];
else $last_name = "";
$encoded_params .= ";last_name=".$last_name;

if ( isset($_POST['email']) )
	$rec_mail = $_POST['email'];
else $rec_mail = "s.buckpesch@iconsultants.eu";
$encoded_params .= ";email=".$rec_mail;

$encoded_params = base64_encode($encoded_params);


$confirmation_link = "http://www.schaebens.de/de/newsletter-anmeldung?params=" . $encoded_params;

require 'libs/PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->SetLanguage("de", "../PHPMailer/");
$mail->CharSet = "UTF-8";
$mail->Host = 'smtp.mandrillapp.com';  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 's.buckpesch@iconsultants.eu';                            // SMTP username
$mail->Password = 'wLVRCnIu1AoqnvSutYuABg';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'info@schaebens.de';
$mail->FromName = 'Schaebens Newsletter Anmeldung';
$mail->addAddress($rec_mail, $first_name . " " . $last_name);  // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo('info@schaebens.de', 'Schaebens Newsletter Anmeldung');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

//$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
/*$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML*/

$mail->Subject = 'Bitte bestätige die Newsletter Anmeldung';
$mail->Body    = $salutation. ' ' . $first_name. ',

du bist nur noch einen Schritt von deinen regelmäßigen Schönheitstipps und Produktnews aus dem Hause Schaebens entfernt. Bitte bestätige durch Anklicken des  folgenden Links, dass du unseren kostenlosen Newsletter abonnieren möchtest:

'. $confirmation_link . '
 
Sobald du den Link bestätigt hast, erhältst du einmal im Monat unseren Gratis- Newsletter, in dem wir dich mit spannenden Neuheiten aus der Beauty-Welt von Schaebens informieren. Selbstverständlich kannst du diesen auch jederzeit wieder abbestellen. Dazu einfach den entsprechenden Abmeldelink am Ende eines jeden Newsletters nutzen.

 
Datenschutz & Co.
---
Durch das doppelte Bestätigungsverfahren (Double-Opt-In) sind deine Daten bei uns sicher. Wurde deine E-Mailadresse ohne dein Wissen für den Schaebens Newsletter angegeben, bitten wir dich, diese E-Mail zu ignorieren.
 
Wir freuen uns über deine Anmeldung für den Schaebens Beauty-Newsletter und wünschen dir viel Freude beim Lesen und Wellnessen!

Dein Schaebens Team
';
//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

//echo 'Message has been sent';
?>

<!doctype html>
<html lang="de-DE">
<head>
    <meta charset="UTF-8">
    <title>Newsletter App</title>
    <link rel="stylesheet" href="assets/vendor/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

<section id="page" class="container">


    <div class="content_danke">

      </div>



</section>





<script src="assets/vendor/js/jquery.js"></script>
<script src="assets/vendor/js/bootstrap.js"></script>
</body>
</html>

